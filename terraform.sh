#!/bin/bash

# Run the command and ignore the exit code
command || true

# Set AWS & remote backend variables
account="disaster"
account_id="606751294606"
bucket="bucket=datakeeper-tf-state-${account}"
backend="dynamodb_table=datakeeper-tf-state-${account}"
role="role_arn=arn:aws:iam::${account_id}:role/OrganizationAccountAccessRole"

# Set workspace
TF_WORKSPACE="dk"

# Set and source variables from .env file
set -a
source .env
set +a

# Initialize and select the Terraform workspace
init_workspace() {
  terraform init \
    -backend-config=$bucket \
    -backend-config=$backend \
    -backend-config=$role \
    -input=false \
    -reconfigure

  # If the workspace exists, select it. If not, create a new one.
  if terraform workspace list | grep -q "$TF_WORKSPACE"; then
    terraform workspace select "$TF_WORKSPACE"
  else
    terraform workspace new "$TF_WORKSPACE"
  fi
}

# Handling arguments passed to the script
case "$1" in
  plan)
#    terraform force-unlock -force 0c33db34-7986-9dd0-9c8a-6f5d7f236b05
    init_workspace
#    terraform state list
#    terraform console
#    terraform import module.central_logs_proxy_issuer_sub_filter[0].aws_iam_role.default[0] disaster-dk-central-logs-proxy-issuer
#    terraform import module.central_logs_proxy_issuer_sub_filter[0].aws_iam_policy.default[0] disaster-dk-central-logs-proxy-issuer
#    terraform validate
    terraform plan -out=plan #-refresh=false
    ;;
  apply)
    init_workspace
#    terraform plan -out=plan -refresh=false
    terraform apply -auto-approve plan
    ;;
  destroy)
    init_workspace
#    terraform plan -out=plan -destroy -refresh=false
#    terraform apply -auto-approve plan
    ;;
  *)
    echo "Invalid argument. Use one of: plan, apply, destroy."
    exit 1
    ;;
esac

##!/bin/bash
#
## AWS
#account="disaster"
#account_id="606751294606"
#bucket="bucket=datakeeper-tf-state-${account}"
#backend="dynamodb_table=datakeeper-tf-state-${account}"
#role="role_arn=arn:aws:iam::${account_id}:role/OrganizationAccountAccessRole"
#
## Set workspace
#TF_WORKSPACE="dk"
#
## Set variables
#if [ "$TF_WORKSPACE" = dk ]; then
#  export TF_VAR_ALARMS_EMAIL_GROUP="malnuaimi@mobiquityinc.com"
#  export TF_VAR_ANDROID_PACKAGE_ID="test"
#  export TF_VAR_ANDROID_PACKAGE_NAME="test"
#  export TF_VAR_AWS_LOGS_REGION="eu-west-1"
#  export TF_VAR_DATAKEEPER_APP_ID="test"
#  export TF_VAR_DELETE_PERSONAL_DATA="false"
#  export TF_VAR_DOCDB_INSTANCE_CLASS="db.r6g.large"
#  export TF_VAR_DYNAMODB_EXTERNAL_TABLE_NAME="devdk-dk-thirdparty"
#  export TF_VAR_DYNAMODB_THIRDPARTY_ITEM_NAME="Disaster"
#  export TF_VAR_DYNAMODB_USE_EXTERNAL="false"
#  export TF_VAR_ENABLE_ALB_ACCESS_LOGS="false"
#  export TF_VAR_ENABLE_CERT_EXPIRY_NOTIF="false"
#  export TF_VAR_ENABLE_CLOUDFRONT_LOGS="false"
#  export TF_VAR_ENABLE_CLOUDTRAIL="true"
#  export TF_VAR_ENABLE_COGNITO="false"
#  export TF_VAR_ENABLE_DISASTER_RECOVERY="false"
#  export TF_VAR_ENABLE_DOCUMENTDB="true"
#  export TF_VAR_ENABLE_DYNAMODB="true"
#  export TF_VAR_ENABLE_ELASTICSEARCH="false"
#  export TF_VAR_ENABLE_IDP="false"
#  export TF_VAR_ENABLE_ISSUER_DIRECTORY="true"
#  export TF_VAR_ENABLE_LOCAL_DYNAMO_ITEM="false"
#  export TF_VAR_ENABLE_MOCK="false"
#  export TF_VAR_ENABLE_PORTAL="false"
#  export TF_VAR_ENABLE_VERIFICATION_SITE="false"
#  export TF_VAR_ENABLE_DEEPLINK_SITE="false"
#  export TF_VAR_ENABLE_PROXY_ISSUER="true"
#  export TF_VAR_ENABLE_RDS_POSTGRES="false"
#  export TF_VAR_ENABLE_SSI_API="false"
#  export TF_VAR_ENABLE_VPC_FLOW_LOGS="false"
#  export TF_VAR_ENABLE_WAF="false"
#  export TF_VAR_ES_INSTANCE_TYPE="r6g.large.elasticsearch"
#  export TF_VAR_FORCE_DESTROY="true"
#  export TF_VAR_IDA_LOG_LEVEL="DEBUG"
#  export TF_VAR_INNOVALOR_RETRY_ATTEMPTS="5"
#  export TF_VAR_INNOVALOR_RETRY_DELAY_MS="500"
#  export TF_VAR_IOS_BUNDLE_ID="test"
#  export TF_VAR_ISSUER_DIRECTORY_ACCOUNT="377175949301"
#  export TF_VAR_ISSUER_PUBLIC_ADDRESS=""
#  export TF_VAR_LIFECYCLE_RULE_ENABLED="true"
#  export TF_VAR_SALTEDGE_APP_ID="test"
#  export TF_VAR_SALTEDGE_RETURN_TO_URL="test"
#  export TF_VAR_SESSION_TTL_SECONDS="3600"
#  export TF_VAR_SIGNICAT_IDIN_REDIRECT_URI="test"
#  export TF_VAR_SNS_QUOTA_LIMIT="1"
#  export TF_VAR_SALTEDGE_PRIVATE_KEY="test"
#  export TF_VAR_TIX_API_KEY="test"
#  export TF_VAR_TIX_BASE_URL="test"
#  export TF_VAR_SALTEDGE_SECRET="test"
#  export TF_WORKSPACE="${TF_WORKSPACE}"
#  export AWS_PROFILE="terraform-master"
#
#  echo "Variables exported..."
#fi
#
## Conditional plan/apply argument
## Run plan first
#if [ "$1" = plan ];
#    then
#    shift
##        terraform force-unlock -force deccb198-47b5-0646-1c81-b62207dffd0b
#        terraform workspace select $TF_WORKSPACE || terraform workspace new $TF_WORKSPACE
##        terraform state list
#        terraform init -backend-config=$bucket -backend-config=$backend -backend-config=$role -reconfigure #-upgrade
##        terraform console
##        terraform providers
##        infracost breakdown --path . --format json --out-file cost.json
##        infracost output --format azure-repos-comment --path cost.json
##        infracost breakdown --sync-usage-file --usage-file infracost-usage.yml --path ./
##        infracost breakdown --path ./ --usage-file infracost-usage.yml
##        First: Remove the state after it has been restored with AWS Backup
##        terraform state rm 'modules.issuer_directory_ssm.aws_ssm_parameter.default'
##        terraform state rm 'aws_dynamodb_table_item.dynamodb_whitelist_urls_entry'
##        terraform state rm 'module.dynamodb_whitelist_urls_table.aws_dynamodb_table.default[0]'
##        terraform state rm 'module.dynamodb_thirdparty_table.aws_dynamodb_table.default[0]'
##        terraform state rm 'module.dynamodb_document_able.aws_dynamodb_table.default[0]'
##        terraform state rm 'module.rds_postgres_cluster.aws_rds_cluster.primary[0]'
##        terraform state rm 'module.documentdb_cluster.aws_docdb_subnet_group.default[0]'
##        terraform state rm 'module.documentdb_cluster.aws_docdb_cluster.default[0]'
##        terraform state rm 'module.documentdb_cluster.aws_docdb_cluster_instance.default[0]'
##        terraform state rm 'module.documentdb_cluster.aws_docdb_cluster_instance.default[1]'
##        Second: Import back the table (Only import don't run plan) and run the pipeline from Azure
##        terraform import aws_s3_object.static_files s3://disaster-dk-static/[\"\"]
##        terraform state rm $(terraform state list | grep aws_s3_object)
##        terraform import module.dynamodb_thirdparty_table.aws_dynamodb_table.default[0] disaster-dk-thirdparty
##        terraform import module.dynamodb_document_table.aws_dynamodb_table.default[0] disaster-dk-document
##        terraform import module.rds_postgres_cluster.aws_rds_cluster.primary[0] disaster-dk-portaldb
##        DocumentDB
##        Before the following you first need to attach instances to the cluster
##        https://docs.aws.amazon.com/aws-backup/latest/devguide/restoring-docdb.html
##        terraform import module.documentdb_cluster.aws_docdb_subnet_group.default[0] disaster-dk-ssi-db
##        terraform import module.documentdb_cluster.aws_docdb_cluster.default[0] disaster-dk-ssi-db
##        terraform import module.documentdb_cluster.aws_docdb_cluster_instance.default[0] disaster-dk-ssi-db-1
##        terraform import module.documentdb_cluster.aws_docdb_cluster_instance.default[1] disaster-dk-ssi-db-2
##        module.dk_portal_cognito.aws_cognito_user_pool.pool[0]
##        terraform import module.dk_cloudtrail_cis_alarms.module.sns_kms_key[0].aws_kms_alias.default[0] alias/disaster-dk-cloudtrail-sns
##        terraform import module.dk_cloudtrail_cis_alarms.module.sns_kms_key[0].aws_kms_key.default[0] alias/disaster-dk-cloudtrail-sns
##        terraform import module.dynamodb_thirdparty_table_cloudtrail_alert.module.sns_kms_key[0].aws_kms_alias.default[0] alias/disaster-dk-cloudtrail-sns
##        terraform import module.vpn_endpoint.aws_ec2_client_vpn_authorization_rule.all_groups[0] cvpn-endpoint-0a0a8f08cbddd89e9,172.16.2.0/24
##        terraform show -json plan >> plan.json
##        terraform output identity_api_secret
##        terraform output master_public_address
#        terraform plan -out=plan
#elif [ "$1" = apply ];
#    then
#    shift
#        terraform init -backend-config=$bucket -backend-config=$backend -backend-config=$role -reconfigure
#        terraform plan -out=plan
#        terraform apply --auto-approve plan
#elif [ "$1" = destroy ];
#    then
#    shift
##        terraform force-unlock -force 10d40829-22d7-c655-3ad5-0bd125cb7f3e
#        terraform init -backend-config=$bucket -backend-config=$backend -backend-config=$role -reconfigure -upgrade
#        terraform plan -out=plan -destroy
#        terraform apply --auto-approve plan
#fi