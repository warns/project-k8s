locals {
  source_base_url = "git::https://github.com/terraform-aws-modules/terraform-aws-vpc.git//?ref=v4.0.2"
}

inputs = {
  name = "eks-vpc"

  azs = [
    "eu-west-1a",
    "eu-west-1b",
#    "eu-west-1c"
  ]

  enable_nat_gateway = false
  //  single_nat_gateway = false # Lets use Transit Gateway
  //  reuse_nat_ips       = true # Lets use Transit Gateway
  //  external_nat_ip_ids = [dependency.eip_nat.outputs.eip_ids.1] # Lets use Transit Gateway

  enable_dns_hostnames = true
  enable_dns_support = true

  private_subnet_tags = {
    Name = "eks-vpc-private-subnet"
  }

  eks_subnet_tags = {
    Name = "eks-vpc-eks-subnet"
  }

  private_route_table_tags = {
    Name = "eks-vpc-private-rt"
  }

  tags = {
    Name = "eks-vpc"
  }
}